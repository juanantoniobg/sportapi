import mainComponent from "../layout/mainLayout.vue";
import players from "../pages/player/Player";
import teams from "../pages/team/Team";
import login from "../pages/auth/Login";

const routes = [
    {
        path: "/",
        component: mainComponent,
        redirect: "/players"
    }, {
        name: "players",
        path: "/players",
        component: players,
        meta: {
            middlewareAuth: true
        }
    }, {
        name: "teams",
        path: "/teams",
        component: teams,
        meta: {
            middlewareAuth: true
        }
    }, {
        name: "login",
        path: "/login",
        component: login
    }
];

export default routes;
