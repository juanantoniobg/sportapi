window.$ = window.jQuery = require("jquery");
// Axios configuration
window.axios = require("axios");
window.api_route = 'https://bf473f5c.ngrok.io';
require("materialize-css");
// Auth configuration
import Api from "./api.js";
import Auth from "./auth.js";
window.api = new Api();
window.auth = new Auth();
window.Event = new Vue();

import Vue from "vue";
import VueRouter from "vue-router";

import App from "./App.vue";
import routes from "./routes";

Vue.use(VueRouter);

const router = new VueRouter({routes, linkActiveClass: "active", props: true});
// Auth Middleware
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.middlewareAuth)) {
        if (!auth.check()) {
            next({
                path: "/login",
                query: {
                    redirect: to.fullPath
                }
            });
            return;
        }
    }
    next();
});

window.app = new Vue({
    el: "#app",
    render: h => h(App),
    router,
    data: {
        authenticated: auth.check(),
        user: auth.user,
        auth: auth,
        loading: false
    },
    mounted: function () {
        $(".dropdown-trigger").dropdown();
        $(".sidenav").sidenav();
        Event.$on("userLoggedIn", () => {
            this.authenticated = true;
            this.user = this.auth.user;
        });
    },
    methods: {}
});
