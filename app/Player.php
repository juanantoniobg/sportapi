<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $table = 'players';

    public $fillable = [
        'first_name',
        'last_name',
        'team_id'
    ];

    function team(){
        return $this->hasOne('App\Team', 'id', 'team_id');
    }
}
