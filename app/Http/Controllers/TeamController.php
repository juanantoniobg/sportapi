<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\Http\Resources\TeamsCollection;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\TeamsResource;
use App\Http\Resources\TeamsPlayersCollection;


class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('teams');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validator->fails()) {
            return collect([
                'data' => [
                    'status' => 'error',
                    'message' => $validator->errors()
                ]
            ]);
        }
        try {
            $team = Team::create($request->all());
            return collect([
                'data' => [
                    'status' => 'success',
                    'message' => 'Team registration successfully',
                    'team' => new TeamsResource(Team::find($team->id))
                ]
            ]);
        } catch (\Exception $e) {
            return collect([
                'data' => [
                    'status' => 'error',
                    'message' => 'An error ocurred' . $e->getMessage()
                ]
            ]);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return collect([
                'data' => [
                    'status' => 'error',
                    'message' => $validator->errors()
                ]
            ]);
        }
        try {
            $team = Team::find($request->input('id'));
            $team->name = $request->input('name');
            $team->save();
            return collect([
                'data' => [
                    'status' => 'success',
                    'message' => 'Team info edited successfully'
                ]
            ]);
        } catch (\Exception $e) {
            return collect([
                'data' => [
                    'status' => 'error',
                    'message' => 'An error ocurred'
                ]
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        if ($validator->fails()) {
            return collect([
                'data' => [
                    'status' => 'error',
                    'message' => $validator->errors()
                ]
            ]);
        }
        try {
            Team::find($request->input('id'))->delete();
            return collect([
                'data' => [
                    'status' => 'success',
                    'message' => 'Team deleted successfully'
                ]
            ]);
        } catch (\Exception $e) {
            return collect([
                'data' => [
                    'status' => 'error',
                    'message' => 'An error ocurred'
                ]
            ]);
        }
    }


    /**
     * Retrieve teams in json format.
     *
     * @param  int  $id
     * @return array
     */
    public function getTeams()
    {
        // $players = Player::with('team')->get();
        // return $players;
        return new TeamsCollection(Team::get());
    }

    /**
     * Retrieve teams in json format.
     *
     * @param  int  $id
     * @return array
     */
    public function getTeamsPlayers()
    {
        // $players = Player::with('team')->get();
        // return $players;
        return new TeamsPlayersCollection(Team::with('players')->get());
    }
}
