<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TokenGenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tokengen');
    }

    /**
     * Generate personal access token.
     *
     * @param  int  $id
     * @return array
     */
    public function requestToken()
    {
        $user = auth()->user();
        $token = $user->createToken('Token-' . $user->name, ['*'])->accessToken;
        return $token;
    }
}
