<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Player;
use App\Http\Resources\PlayersCollection;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\PlayersResource;

class PlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('players');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'team_id' => 'required'
        ]);
        if ($validator->fails()) {
            return collect([
                'data' => [
                    'status' => 'error',
                    'message' => $validator->errors()
                ]
            ]);
        }
        try {
            $player = Player::create($request->all());
            return collect([
                'data' => [
                    'status' => 'success',
                    'message' => 'Player registration successfully',
                    'player' => new PlayersResource(Player::with('team')->find($player->id))
                ]
            ]);
        } catch (\Exception $e) {
            return collect([
                'data' => [
                    'status' => 'error',
                    'message' => 'An error ocurred'.$e->getMessage()
                ]
            ]);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'team_id' => 'required'
        ]);
        if ($validator->fails()) {
            return collect([
                'data' =>[
                    'status' => 'error',
                    'message' => $validator->errors()
                ]
            ]);
        }
        try {
            $player = Player::find($request->input('id'));
            $player->first_name = $request->input('first_name');
            $player->last_name = $request->input('last_name');
            $player->team_id = $request->input('team_id');
            $player->save();
            return collect([
                'data' => [
                    'status' => 'success',
                    'message' => 'Players info edited successfully'
                ]
            ]);
        }catch(\Exception $e){
            return collect([
                'data' => [
                    'status' => 'error',
                    'message' => 'An error ocurred'
                ]
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        if ($validator->fails()) {
            return collect([
                'data' => [
                    'status' => 'error',
                    'message' => $validator->errors()
                ]
            ]);
        }
        try {
            Player::find($request->input('id'))->delete();
            return collect([
                'data' => [
                    'status' => 'success',
                    'message' => 'Players deleted successfully'
                ]
            ]);
        } catch (\Exception $e) {
            return collect([
                'data' => [
                    'status' => 'error',
                    'message' => 'An error ocurred'
                ]
            ]);
        }
    }


    /**
     * Retrieve players in json format.
     *
     * @param  int  $id
     * @return array
     */
    public function getPlayers()
    {
        return new PlayersCollection(Player::with('team')->get());
    }
}
