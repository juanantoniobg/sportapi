<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PlayersCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function($player){
            return collect([
                'id' => $player->id,
                'first_name' => $player->first_name,
                'last_name' => $player->last_name,
                'team_id' => $player->team_id,
                'team' => collect([
                    'id' => $player->team->id,
                    'name' => $player->team->name,
                ])
            ]);
        });
    }
}
