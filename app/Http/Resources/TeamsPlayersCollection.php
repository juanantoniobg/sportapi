<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TeamsPlayersCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function($team){
            return collect([
                'id' => $team->id,
                'name' => $team->name,
                'players' => $team->players->map(function($player){
                    return collect([
                        'id' => $player->first_name,
                        'first_name' => $player->first_name,
                        'last_name' => $player->last_name
                    ]);
                })
            ]);
        });
    }
}
