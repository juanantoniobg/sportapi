@extends('layout.main')
@section('content')
<div class="row">
        <div class="col m8 offset-2">
            <passport-authorized-clients></passport-authorized-clients>
            <passport-clients></passport-clients>
            <passport-personal-access-tokens></passport-personal-access-tokens>
        </div>
</div>
@endsection
