 {{-- <nav>
        <div class="nav-wrapper">
        <a href="#" class="brand-logo">SportAPI</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="{{ route('players_index') }}">Players</a></li>
            <li><a href="{{ route('teams_index') }}">Teams</a></li>
        </ul>
        </div>
    </nav> --}}


    <nav>
    <div class="nav-wrapper">
      <a href="#!" class="brand-logo">SportAPI</a>
      <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li class="@if(url()->current() == route('players_index')) active @endif"><a href="{{ route('players_index') }}">Players</a></li>
        <li class="@if(url()->current() == route('teams_index')) active @endif"><a href="{{ route('teams_index') }}">Teams </a></li>
        <li class="@if(url()->current() == route('token_gen_index')) active @endif"><a href="{{ route('token_gen_index') }}">Api Access Token</a></li>
        <li>
              <a class='dropdown-trigger btn' href='#' data-target='dropdown1'>
                  {{ auth()->user()->name }}
              </a>
        </li>
      </ul>
    </div>
  </nav>

  <ul class="sidenav" id="mobile-demo">
        <li><h5 align="center">SportAPI</h5></li>
        <li><a href="{{ route('players_index') }}">Players</a></li>
        <li><a href="{{ route('teams_index') }}">Teams</a></li>
        <li><a href="{{ route('token_gen_index') }}">Api Access Token</a></li>
        <li><a href="{{ route('logout') }}">Logout</a></li>
  </ul>



  <!-- Dropdown Structure -->
  <ul id='dropdown1' class='dropdown-content'>
    <li><a href="{{ route('logout') }}">Logout</a></li>
  </ul>
