require("./bootstrap");
require("materialize-css");
import Vue from "vue";

// Passport componenets
Vue.component("passport-authorized-clients", require("./components/passport/AuthorizedClients.vue"));
Vue.component("passport-clients", require("./components/passport/Clients.vue"));
Vue.component("passport-personal-access-tokens", require("./components/passport/PersonalAccessTokens.vue"));

var app = new Vue({
    el: "#app",
    components: {
        login: require("./components/auth/Login.vue"),
        players: require("./components/player/Player.vue"),
        teams: require("./components/team/Team.vue"),
        accesstoken: require("./components/accesstoken/AccessToken.vue")
    },
    mounted: function () {
        $(".dropdown-trigger").dropdown();
        $(".sidenav").sidenav();
        $(".collapsible").collapsible();
    },
    data: {
        loading: false
    }
});
