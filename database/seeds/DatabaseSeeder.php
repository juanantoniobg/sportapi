<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->command->info('Seeding Team table!');
        $this->call(TeamTableSeeder::class);
        $this->command->info('Team tables seeded!');
        $this->command->info('Seeding Player table!');
        $this->call(PlayerTableSeeder::class);
        $this->command->info('Player tables seeded!');

    }
}
