<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Team;

class TeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->delete();
        Team::create([
            'name' => 'Manchester United'
        ]);
        Team::create([
            'name' => 'Manchester City'
        ]);
        Team::create([
            'name' => 'Liverpool'
        ]);
    }
}
