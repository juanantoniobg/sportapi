<?php

use Illuminate\Database\Seeder;
use App\Player;

class PlayerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('players')->delete();
        Player::create([
            'first_name' => 'Wayne',
            'last_name' => 'Rooney',
            'team_id' => 1
        ]);
        Player::create([
            'first_name' => 'Sergio',
            'last_name' => 'Agüero',
            'team_id' => 2
        ]);
        Player::create([
            'first_name' => 'Mohamed',
            'last_name' => 'Salad',
            'team_id' => 3
        ]);
    }
}
