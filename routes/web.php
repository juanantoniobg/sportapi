<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return ['SportAPI frontend: ' => 'https://c8296e41.ngrok.io'];
});


// Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login_form');
// Route::post('/login', 'Auth\LoginController@login')->name('login');
// Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

// Route::group(['middleware' => 'auth:web'], function(){

//     // Player by default.
//     Route::get('/', 'PlayerController@index')->where('any', '.*');

//     // Players
//     Route::get('/players', 'PlayerController@index')->name('players_index');
//     Route::get('/players/get', 'PlayerController@getPlayers')->name('players_get');
//     Route::post('/players/update', 'PlayerController@update')->name('players_update_ajax');
//     Route::post('/players/store', 'PlayerController@store')->name('players_store_ajax');
//     Route::post('/players/destroy', 'PlayerController@destroy')->name('players_destroy_ajax');

//     // Teams
//     Route::get('/teams', 'TeamController@index')->name('teams_index');
//     Route::get('/teams/get', 'TeamController@getTeamsPlayers')->name('teams_get');
//     Route::post('/teams/update', 'TeamController@update')->name('teams_update_ajax');
//     Route::post('/teams/store', 'TeamController@store')->name('teams_store_ajax');
//     // Route::post('/teams/destroy', 'TeamController@destroy')->name('teams_destroy_ajax'); // Disabled.

//     // Api acces
//     Route::get('/tokengen', 'TokenGenController@index')->name('token_gen_index');
//     Route::get('/tokengen/request', 'TokenGenController@requestToken')->name('token_gen_request');

// });
