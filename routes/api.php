<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::get('unauthorized', function () {
    return response()->json([
        'message' => 'Unauthorized',
        'status' => 401
    ], 401);
})->name('unauthorized');

Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

  // These routes use OAuth 2.O
Route::group(['middleware' => 'auth:api'], function () {

    // Get user data for login
    Route::get('/get-user', 'Auth\LoginController@getUser');

     // Players
    Route::get('/players', 'PlayerController@index')->name('players_index');
    Route::get('/players/get', 'PlayerController@getPlayers')->name('players_get');
    Route::post('/players/update', 'PlayerController@update')->name('players_update_ajax');
    Route::post('/players/store', 'PlayerController@store')->name('players_store_ajax');
    Route::post('/players/destroy', 'PlayerController@destroy')->name('players_destroy_ajax');

    // Teams
    Route::get('/teams', 'TeamController@index')->name('teams_index');
    Route::get('/teams/get', 'TeamController@getTeamsPlayers')->name('teams_get');
    Route::post('/teams/update', 'TeamController@update')->name('teams_update_ajax');
    Route::post('/teams/store', 'TeamController@store')->name('teams_store_ajax');
    // Route::post('/teams/destroy', 'TeamController@destroy')->name('teams_destroy_ajax'); // Disabled.
});
